# Data analysis library
import pandas as pd # E R(enzie) keffaaaa
# Database library
import sqlite3 as sql

class TA:
    def __init__(self, db):
        self.db =  sql.connect(db)
        self.c = self.db.cursor()
    
    def __del__(self):
        self.db.close()
    
    # Data is a dictionary of samples labelled according to database columns
    def put(self, data):
        labels = ', '.join(tuple(map(str,data.keys())))
        values = ', '.join(tuple(map(str,data.values())))

        query = "INSERT INTO hourly (%s) VALUES (%s)" % (labels, values)
        
        self.c.execute(query)
        self.db.commit() # save data
        
    def get(self):
        values = pd.read_sql("SELECT * FROM hourly WHERE datetime >= datetime('now', '-1 day') ORDER BY datetime ASC LIMIT 300;", self.db)
        return values