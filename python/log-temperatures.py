import DomoticGateway as dg
import TemperatureArchive as ta

import subprocess
from time import sleep
import sys
import pandas as pd
import matplotlib as mpl
mpl.use('Agg') # disable plot display
import matplotlib.pyplot as plt
import matplotlib.dates as pld

# TMP is in different database, this is stupid: merge all in a single database
HDD_LABELS = ('sda','sdb','sdc','sdd')
CORE_LABELS = ('core0', 'core1', 'core2', 'core3')
TMP_LABELS_AMB = ('ambtmp',)

# Remote codes
CLIMA_CODES = {'freddo' : 0x4dc0bf, 'caldo' : 0x4d4033, 'spegni' : 0x4d841f}

# Read hard disk temperatures and put in array
def getDiskTmp():
  tmp = {}
  for l in HDD_LABELS:
    status,output = subprocess.getstatusoutput("hddtemp -n /dev/%s" % l)
    if status == 0:
      tmp[l] = output
    else:
      tmp[l] = None
  return tmp


def getCoreTmp():
  status,output = subprocess.getstatusoutput("sensors -u | grep 'temp[0-9]_input:' | cut -d':' -f2")
  tmp = output.replace(' ','').split('\n')
  if len(tmp) == len(CORE_LABELS):
    return dict(zip(CORE_LABELS, tmp))
  else:
    return {i:None for i in CORE_LABELS}
  

def getRoomTmp():
  dgh = dg.DomoticGateway()
  dgh.sendCommand('CMD_TEMP')
  try:
    tmp = dgh.receiveData()
  except Exception as e:
    tmp = None

  return dict(zip(TMP_LABELS_AMB, [tmp]))

def setClimaStatus(status):
  dgh = dg.DomoticGateway()
  try:
    status = CLIMA_CODES[status]
  except:
    print ("Invalid clima option")
    exit(-1)
  # Clima packetis 4 bytes (uint32_t)
  payload = []
  payload.append(status & 0xff)
  status >>= 8
  payload.append(status & 0xff)
  status >>= 8
  payload.append(status & 0xff)
  payload.append(0x00)
  dgh.sendCommand('CMD_CLIMA', payload)

def plotData(data, labels, name):
  fig, ax = plt.subplots(nrows=1, ncols=1)
  for i in labels:
    ma = data[i].rolling(10).mean() # calculate moving average
    ax.plot(data['datetime'], ma, linewidth=1.5, label=i)
  ax.axvline('00:00', color='k', linestyle='--')
  xfmt = pld.DateFormatter('%H:%M')
  ax.xaxis.set_major_formatter(xfmt)
  ax.legend()
  fig.savefig(name)

def main():
  if (len(sys.argv) != 3):
    # Insert a new row in database
    archive = ta.TA('temperatures.db')
    disk = getDiskTmp()
    core = getCoreTmp()
    amb  = getRoomTmp()
    archive.put({**disk, **core, **amb})

    # Fetch last day history and plot
    tmp = archive.get()
    tmp['datetime'] = pd.to_datetime(tmp['datetime'])
    tmp.set_index('datetime')

    plotData(tmp, HDD_LABELS, 'plots/hdd.jpg')
    plotData(tmp, CORE_LABELS, 'plots/core.jpg')
    plotData(tmp, TMP_LABELS_AMB, 'plots/amb.jpg')

  else:
    if (sys.argv[1] == 'clima'):
      setClimaStatus(sys.argv[2])

  # push into database
  # read db to plot



if __name__ == "__main__":
  main()
