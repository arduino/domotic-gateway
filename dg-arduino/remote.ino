#include <TimerOne.h>
#include "remote.h"

// Function calls to make mark (PWM pulses) and space (LOW)
#define MARK()  Timer1.setPwmDuty(9, 472)
#define SPACE() Timer1.setPwmDuty(9, 0)

const uint8_t PWM_PIN = 9;

// Timings
const int PWM_PERIOD         = 26;
const int START_PULSE_LENGTH = 4455;
const int MARK_LENGTH        = 560;
const int ZERO_SPACE_LENGTH  = 1635;
const int ONE_SPACE_LENGTH   = 537;


void initRemote() {
  Timer1.initialize(PWM_PERIOD);
  Timer1.pwm(PWM_PIN, 0);
  pinMode(PWM_PIN, OUTPUT);
}

void sendBit(boolean b) {
  MARK();
  delayMicroseconds(MARK_LENGTH);
  SPACE();
  delayMicroseconds(b ? ONE_SPACE_LENGTH : ZERO_SPACE_LENGTH);
}

void sendChunk(uint8_t chunk) {
  for (uint8_t c = 0; c < 8; c++) {
    sendBit(chunk & _BV(7));
    chunk <<= 1;
  }
}

void sendData(uint32_t data) {
  // Header
  MARK();
  delayMicroseconds(START_PULSE_LENGTH);
  SPACE();
  delayMicroseconds(START_PULSE_LENGTH);
  // Data
  sendChunk((data & 0xff0000) >> 16);
  sendChunk((~data & 0xff0000) >> 16);
  sendChunk((data & 0x00ff00) >> 8);
  sendChunk((~data & 0x00ff00) >> 8);
  sendChunk(data & 0x0000ff);
  sendChunk(~data & 0x0000ff);
  sendBit(0);
}

void sendCommand(uint32_t data) {
  // Send it twice
  sendData(data);
  delayMicroseconds(START_PULSE_LENGTH);
  sendData(data);
}

