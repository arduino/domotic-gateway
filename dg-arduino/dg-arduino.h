#ifndef DG_ARDUINO_H_
#define DG_ARDUINO_H_

#define BUF_SIZE 10

enum {CMD_CLIMA, CMD_TEMP, CMD_ERR, CMD_DEBUG};

struct Packet_t {
  uint8_t size;
  uint8_t cmd;

  // Note: at the end a CHK is append and size is automatically updated by SendPacket method
  Packet_t() {
  }

  Packet_t(uint8_t size, uint8_t cmd) {
    this->size = size;
    this->cmd = cmd;
  }
};

struct PackClimaCmd_t : Packet_t {
  uint32_t cmd_clima;
  PackClimaCmd_t() {
    this->size = sizeof(PackClimaCmd_t);
    this->cmd  = CMD_CLIMA;
  }
};

struct PackTempCmd_t : Packet_t {
  uint8_t cmd_temp;
  PackTempCmd_t() {
    this->size = sizeof(PackClimaCmd_t);
    this->cmd  = CMD_TEMP;
    //this->cmd_temp = cmd_temp;
  }
};

struct PackReceived_t : Packet_t {
  uint8_t buffer[BUF_SIZE];
  PackReceived_t() {
    // Uninitalised, will be populated by reading function
  }
};

struct PackSendTemp_t : Packet_t {
  int temp;
  PackSendTemp_t(int temp) {
    this->size = sizeof(PackSendTemp_t);
    this->cmd  = CMD_TEMP;
    this->temp = temp;
  }
};

struct PackError_t : Packet_t {
  uint8_t err_code;
  PackError_t(uint8_t err_code) {
    this->size = sizeof(PackError_t);
    this->cmd  = CMD_CLIMA;
    this->err_code = err_code;
  }
};

struct PackMsg_t : Packet_t {
  uint8_t msg[10];
  PackMsg_t(uint8_t size) {
    this->size = size + sizeof(Packet_t);
    this->cmd  = CMD_DEBUG;
  }
};

void SendPacket(void *_p) {
  uint8_t * p = (uint8_t*)_p;
  uint8_t chk = 0;
  // increase by one to keep CHK in account
  uint8_t size = *p;
  *p = *p + 1;
  for (uint8_t c = 0; c < size; c++) {
    Serial.write(*p);
    chk += *p;
    p++;
  }
  Serial.write(chk);
}

#endif
