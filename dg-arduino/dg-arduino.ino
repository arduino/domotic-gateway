/*
  Domotic Gateway
  by giuliof @ golem.linux.it
*/
#include "dg-arduino.h"
#define TMP_PIN   A0
#define BUTTON_PIN 7

#define MEAN_SIZE 10
// 30 seconds, interval between temperature samples
unsigned long int interval = 30000;

PackReceived_t PackReceived;
int temperatures[10];


void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  Serial.begin(115200); // open a serial connection to your computer
  initRemote();
  
  for (uint8_t c = 0; c < 10; c++)
    temperatures[c] = analogRead(TMP_PIN);
}

void loop() {
  static long int millis_tmp = 0;

  /* * * * * * * * * * * * * * * * * * * *
      Serial Packet Management
   * * * * * * * * * * * * * * * * * * * */
  // Check if new packets are incoming
  if (Serial.available() > 0) {
    static uint8_t PackCHK = 0;
    static uint8_t PackCount = 0;
    uint8_t ch = Serial.read();

    *((uint8_t*)&PackReceived + PackCount++) = ch;

    // Exclude last byte from CHK count
    if (PackCount < PackReceived.size)
      PackCHK += ch;
    // If last byte check chk and parse packet
    else if (PackReceived.size == PackCount) {
      // If chk is correct...
      if (ch == PackCHK) {

        switch (PackReceived.cmd) {
          case CMD_CLIMA: {
            PackClimaCmd_t *cp = (PackClimaCmd_t*)&PackReceived;
            sendCommand(cp->cmd_clima);
          } break;
          
          case CMD_TEMP: {
            float mean_tmp = 0;
            for (uint8_t c = 0; c < MEAN_SIZE; c++) {
            mean_tmp += temperatures[c] / 10.0;
            }
            PackSendTemp_t p(mean_tmp);
            SendPacket(&p);
          } break;
            
          default: {
            PackError_t err(0x42);
            SendPacket(&err);
          } break;
        }

      }
      else {
        PackError_t err(PackCHK);
        SendPacket(&err);
        SendPacket(&PackReceived);
      }
      PackCHK = 0;
      PackCount = 0;

    }
  }

  /* * * * * * * * * * * * * * * * * * * *
      Temperature reading
   * * * * * * * * * * * * * * * * * * * */
  long int now_time = millis();
  if (now_time - millis_tmp > interval) {
    static uint8_t tmp_cnt = 0;
    millis_tmp = now_time;
    // get tmp measure and do average
    temperatures[tmp_cnt++] = analogRead(TMP_PIN);
    if (tmp_cnt == MEAN_SIZE)
      tmp_cnt = 0;
  }

  /* * * * * * * * * * * * * * * * * * * *
      Debugging BUTTTON_PIN
   * * * * * * * * * * * * * * * * * * * */
  { static uint8_t oldstatus = HIGH;
    uint8_t newstatus = digitalRead(BUTTON_PIN);
    if (newstatus == LOW && oldstatus == HIGH) {
      PackMsg_t p(5);
      p.msg[0] = 'D';
      p.msg[1] = 'e';
      p.msg[2] = 'b';
      p.msg[3] = 'u';
      p.msg[4] = 'g';
      SendPacket(&p);
      oldstatus = newstatus;
    }
    else if ( newstatus == HIGH && oldstatus == LOW)
      oldstatus = newstatus;
  }
}
